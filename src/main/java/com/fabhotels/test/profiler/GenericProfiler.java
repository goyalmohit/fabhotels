package com.fabhotels.test.profiler;

import com.fabhotels.test.services.CodeMetricsService;
import com.fabhotels.test.services.GenericService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class GenericProfiler {

    @Autowired
    private GenericService genericService;

    @Autowired
    private CodeMetricsService codeMetricsService;

    @Around("@annotation(ExecutionTime)")                                                                   // Pluggable Annotation implementation.
    public Object profile(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.nanoTime();
        Object output = joinPoint.proceed();
        long endTime = System.nanoTime();
        double elapsedTime = (endTime - startTime) / 1000000.0;
        codeMetricsService.saveMetrics(joinPoint.getSignature().getName(), elapsedTime);
        return output;
    }

    /*
    @Pointcut(" execution(* com.fabhotels.test.services.*.*(..))")                                          // This pointcut is defined for all services declared under services package.
    public void allServiceMethods(){}

    @Pointcut(" execution (* com.fabhotels.test.services.GenericService+.*(..))")                          // This pointcut is defined for all methods declared inside GenericService class under services package.
    public void genericServiceMethods(){}

    @Around("allServiceMethods()")
    public Object generalProfiler(ProceedingJoinPoint joinPoint) throws Throwable{
        long startTime = System.nanoTime();
        Object output = joinPoint.proceed();
        long endTime = System.nanoTime();
        double elapsedTime = (endTime - startTime) / 1000000.0;
        codeMetricsService.saveMetrics(joinPoint.getSignature().getName(), elapsedTime);
        return output;
    }

    @Around("genericServiceMethods()")
    public Object profiler(ProceedingJoinPoint joinPoint) throws Throwable{
        long startTime = System.nanoTime();
        Object output = joinPoint.proceed();
        long endTime = System.nanoTime();
        double elapsedTime = (endTime - startTime) / 1000000.0;
        codeMetricsService.saveMetrics(joinPoint.getSignature().getName(), elapsedTime);
        return output;
    }*/

}
