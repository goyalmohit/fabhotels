package com.fabhotels.test.controllers;

import com.fabhotels.test.models.view.CodeMetricsDTO;
import com.fabhotels.test.services.CodeMetricsService;
import com.fabhotels.test.services.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/generic/metrics/")
public class GenericController {

    @Autowired
    private GenericService genericService;

    @Autowired
    private CodeMetricsService codeMetricsService;

    @RequestMapping(value = "/{method}", method = RequestMethod.GET)
    public void computeMetrics(@PathVariable String method) throws InterruptedException {
        genericService.totalExecutionTime(method);
    }

    @RequestMapping(value = "/records", method = RequestMethod.GET)
    public List<CodeMetricsDTO> records() {
        return codeMetricsService.records().stream().map(CodeMetricsDTO::new).collect(Collectors.toList());
    }

}
