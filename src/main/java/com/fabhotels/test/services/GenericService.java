package com.fabhotels.test.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GenericService {

    @Autowired
    private SortingService sortingService;

    @Autowired
    private CodeMetricsService codeMetricsService;

    @Autowired
    private CustomService customService;

    public void totalExecutionTime(String method) throws InterruptedException {
        int[] sortedArray, inputArray = new int[20];
        if (!"CUS".equalsIgnoreCase(method)) {
            inputArray = new int[]{547, 78, 90028, 87, 42, 8, 1006, 7369839, 2, 78978727, 1, 183, 3, 76, 728, 5, 789, 890, 17, 2};
            sortedArray = new int[inputArray.length];
        }
        switch (method) {
            case "BUB":
                sortedArray = sortingService.bubbleSort(inputArray);
                break;
            case "INS":
                sortedArray = sortingService.insertionSort(inputArray);
                break;
            case "SEL":
                sortedArray = sortingService.selectionSort(inputArray);
                break;
            case "CUS1":
                customService.customExecution1();
                break;
            case "CUS2":
                customService.customExecution2();
                break;
            default:
                customService.customExecution1();
        }
    }

}
