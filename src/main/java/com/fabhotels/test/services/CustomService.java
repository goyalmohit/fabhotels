package com.fabhotels.test.services;

import com.fabhotels.test.profiler.ExecutionTime;
import org.springframework.stereotype.Service;

@Service
public class CustomService {

    @ExecutionTime
    public void customExecution1()  {
        int square = 0;
        for(int number = 0; number < 10000; number++){
                square = number * number;
        }
    }

    @ExecutionTime
    public void customExecution2()  {
        int cube = 0;
        for(int number = 1000; number > 0; number--){
            cube = number * number * number;
        }
    }
}
