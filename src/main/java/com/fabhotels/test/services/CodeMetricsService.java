package com.fabhotels.test.services;

import com.fabhotels.test.models.db.CodeMetrics;
import com.fabhotels.test.repositories.CodeMetricsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CodeMetricsService {

    @Autowired
    private CodeMetricsRepository codeMetricsRepository;

    public void saveMetrics(String name, double executionTime) {
        CodeMetrics codeMetrics = new CodeMetrics();
        codeMetrics.setMethodName(name);
        codeMetrics.setExecutionTime(executionTime);
        codeMetricsRepository.save(codeMetrics);
    }

    public List<CodeMetrics> records() {
        return codeMetricsRepository.findAll();
    }
}
