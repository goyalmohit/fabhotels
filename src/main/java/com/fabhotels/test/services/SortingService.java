package com.fabhotels.test.services;

import com.fabhotels.test.profiler.ExecutionTime;
import org.springframework.stereotype.Service;

@Service
public class SortingService {

    @ExecutionTime
    public int[] bubbleSort(int[] inputArray) {
        int temp;
        for (int num : inputArray) {
            for (int j = 1; j < inputArray.length; j++) {
                if (inputArray[j - 1] > inputArray[j]) {
                    temp = inputArray[j - 1];
                    inputArray[j - 1] = inputArray[j];
                    inputArray[j] = temp;
                }
            }
        }
        return inputArray;
    }

    @ExecutionTime
    public int[] insertionSort(int[] inputArray) {
        int count = 0, key = 0, previousIndex = 0;
        for (int nextIndex = 1; nextIndex < inputArray.length; nextIndex++) {
            key = inputArray[nextIndex];
            previousIndex = nextIndex - 1;
            while ((previousIndex > -1) && inputArray[previousIndex] > key) {
                inputArray[previousIndex + 1] = inputArray[previousIndex];
                previousIndex--;
            }
            inputArray[previousIndex + 1] = key;
        }
        return inputArray;
    }

    @ExecutionTime
    public int[] selectionSort(int[] inputArray) {
        int smallestElement, smallestElementIndex, count = 0;
        for (int position = 0; position < inputArray.length - 1; position++) {                      // places that value into its proper location
            smallestElementIndex = position;
            for (int nextIndex = position + 1; nextIndex < inputArray.length; nextIndex++) {       // Find next smallest element in this loop.
                if (inputArray[nextIndex] < inputArray[smallestElementIndex]) {
                    smallestElementIndex = nextIndex;
                }
            }
            smallestElement = inputArray[smallestElementIndex];
            inputArray[smallestElementIndex] = inputArray[position];
            inputArray[position] = smallestElement;
        }
        return inputArray;
    }
}
