package com.fabhotels.test.repositories;

import com.fabhotels.test.models.db.CodeMetrics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CodeMetricsRepository extends JpaRepository<CodeMetrics,Long>{

}
