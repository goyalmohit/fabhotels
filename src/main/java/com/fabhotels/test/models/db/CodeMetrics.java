package com.fabhotels.test.models.db;

import org.joda.time.DateTime;
import org.springframework.data.jpa.domain.AbstractAuditable;

import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@Entity
public class CodeMetrics extends AbstractAuditable<CodeMetrics, Long> {

    private String methodName;
    private double executionTime;

    @PrePersist
    void onCreate() {
        this.setCreatedDate(DateTime.now());
    }

    @PreUpdate
    void onPersist() {
        this.setLastModifiedDate(DateTime.now());
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public double getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(double executionTime) {
        this.executionTime = executionTime;
    }

}
