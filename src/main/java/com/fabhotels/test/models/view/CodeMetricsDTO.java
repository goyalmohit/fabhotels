package com.fabhotels.test.models.view;

import com.fabhotels.test.models.db.CodeMetrics;

public class CodeMetricsDTO {

    private long id;
    private String methodName;
    private double executionTime;

    public CodeMetricsDTO(){}

    public CodeMetricsDTO(CodeMetrics cm) {
        this.id = cm.getId();
        this.methodName = cm.getMethodName();
        this.executionTime = cm.getExecutionTime();
    }

    public long getId() {
        return id;
    }

    public String getMethodName() {
        return methodName;
    }

    public double getExecutionTime() {
        return executionTime;
    }

    public CodeMetrics toEntity(){
        return toEntity(new CodeMetrics());
    }

    public CodeMetrics toEntity(CodeMetrics codeMetrics){
        codeMetrics.setMethodName(this.getMethodName());
        codeMetrics.setExecutionTime(this.getExecutionTime());
        return codeMetrics;
    }
}
